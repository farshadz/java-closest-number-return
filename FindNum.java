import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
/**
 * 
 */

/**
 * @authors: farshad Zamani - 416056,  Mehrdad Ahadi - 379957, Pedram
 *           Aliabbasi - 402802,Ali Hadadi Esfahani - 334244, Reza Babakhani - 399870
 *
 * @Title : Array binary search using recursion
 * @Description :Array will be randomly populated with unique numbers. Array
 *              will be sorted -The closest value to the user input will be
 *              returned from Binary search which is using recursion function
 * @Date : 24.10.2016
 *
 */
public class FindNum {

		public static void main(String[] args) {

			// Call for a function to populate the array and show to user
			int[] randomArr = new int[20];
			populateArray(randomArr);
			System.out.println("Randomized Sorted Array : " + Arrays.toString(randomArr));

			for (;;) {

				// Receive the return value of closest index and respective value
				// and show to user
				int foundIndex = binarySearch(randomArr, getInput(randomArr));
				int foundValue = randomArr[foundIndex];
				System.out.println("the closest index is : " + foundIndex + " , with value : " + foundValue);

			}
		}

		/*
		 * Following function add all 0-99 into arraylist(pool) then it randomly
		 * choose a number from 0-99 which is pool size and finally it add the
		 * values into array (myIntArray) and remove the the token value from
		 * arraylist. This algorithm is used to avoid duplicate values in array
		 */
		public static void populateArray(int[] arrToPopulate) {
			ArrayList<Integer> pool = new ArrayList<Integer>();
			for (int i = 0; i < 99; ++i) {
				pool.add(i + 1);
			}
			Random rand = new Random();
			for (int i = 0; i < 19; ++i) {
				int randNum = rand.nextInt(pool.size());
				arrToPopulate[i] = pool.get(randNum);
				pool.remove(randNum);
			}

			Arrays.sort(arrToPopulate);
		}

		/*
		 * The following function is used to get input from the user or exit the
		 * console based on user request
		 */
		private static int getInput(int[] arrToFindIn ) {
			try {
				Scanner reader;
				int input = -1;
				reader = new Scanner(System.in);
				System.out.print("Enter a valid integer or press 'Q' to exit: ");
				String key = reader.nextLine();
				if (key.equals("Q")) {
					System.out.println("Bye .. ");
					System.exit(0);
				} 
				input = Integer.parseInt(key);

				if (input > arrToFindIn[19] || input < arrToFindIn[0]) {

					System.out.println("Please enter an integer within " + arrToFindIn[0] + "-" + arrToFindIn[19]);
					getInput(arrToFindIn);
				}
			
				return input;

			} catch (Exception x) {
				System.out.println("Not a valid integer.");
				return getInput(arrToFindIn);
				
			}
		}

		/*
		 * Following function is used to call the binary search
		 */
		public static int binarySearch(int[] a, int target) {
			return binarySearch(a, 0, a.length - 1, target);
		}

		/*
		 * Following function is used for binary search
		 */
		public static int binarySearch(int[] a, int start, int end, int key) {

			if (start <= end) {
				int mid = start + (end - start) / 2; // Dividing the array into two
														// left and right part
				if (key < a[mid]) { // Checking whether the number is located into
									// left part
					if (mid - start == 1) { // check if the end middle of the
											// division and start is ONE, closest
											// number should be chosen
						if (Math.abs(a[start] - key) < Math.abs(a[mid] - key)) {
							return start;
						} else if (Math.abs(a[mid] - key) < Math.abs(a[mid - 1] - key)) {
							return mid;
						} else {
							return mid - 1;
						}
					}
					return binarySearch(a, start, mid, key);// call for recursion in
															// case there are more
															// number to check

				} else if (key > a[mid]) { // checking if the number located into
											// right part

					if (end - mid == 1) {
						if (Math.abs(a[mid] - key) < Math.abs(a[end] - key)) {
							return mid;
						} else {
							return mid + 1;
						}
					}
					return binarySearch(a, mid, end, key);
				} else {
					return mid; // In case of same number in array the same index
								// will be returned
				}
			}

			return -1;

		}

	}
